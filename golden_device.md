# Golden Device

There are two Golden Device to perform the different tests:

- With the first image, the GD acts as DAGroot (GD/root).
- With the second image, the GD acts as packet sniffer (GD/sniffer).

All images can be configured using a script (described below), which allows setting the value of several parameters (e.g., frequency, slotframe size, etc.), or triggering the transmission of a given type of packet (EB, DATA, ACK, etc.).
The commands which allow configuring the images are presented below; the specific set of parameters to be used for each test are specified below.

## GD/root

With this first image, the golden device is a DAGroot.
By using the script, it is possible to configure:

- the number of frequencies (Single frequency or Multiple Frequencies/Channel Hopping)
- the slotframe size
- the type of packet to send/receive (EB, KA, DATA, ACK, DIO, DAO)
- the value of the DAGrank.

The script displays information about the frames the GD/root received from the vendor node.
For example, following the reception of a KA message, the GD/root prints out the information about the ASN the KA was received, and the Time Offset of the vendor node.

The script can also cause GD/root to issue 6P packets (6P_ADD, 6P_DELETE, 6P_RELOCATE, 6P_COUNT, 6P_LIST, 6P_CLEAR).
The GD/root also returns information about the 6P response (e.g. the number of reserved cells in a 6P_COUNT response, the reserved cell list in 6P_LIST response).
The value of the return code field in the 6P response is always printed.
The script also allows the user to specify up to 3 slots to be included in the 6P_ADD or 6P_DELETE packets.

## GD/sniffer

With this image, the golden device acts as a packet sniffer.
The script allows the user to configure the frequency the GD/sniffer is listening on.
The packet sniffer can forward the received frames to the dissector.
The GD/sniffer is mainly used for conformance tests to verify packet formats and the values of specific fields, as detailed in the different tests.

## Configuring Script

A Python configuration script allows the user to configure the golden device.
The script sends command to the GD over its serial port.
The format is presented here for informational purposes:

Command frame is composed by three different fields, as specified below:

    | Length (bytes)           | 1         | 1         | Variable             |
    | ------------------------ | --------- | --------- | -----------------    |
    | Command Content          | CommandId | length    | (value of) Parameter |

CommandID: this field (1 byte long), allows identifying the specific command used for configuring the GD.

Length: this field (1 byte long) specifies the length of the next field, i.e., of the parameter content.

(value of) Parameter: this field contains the value of the specific parameter configured by using that command. The Table below summarizes the list of parameters which can be configured, using different commands (identified by different CommandID).

| Command Scope                   | CommandID | Length                 | Parameter          | Allowed Range of Value                                                   | Unit           |
|---------------------------------|-----------|------------------------|--------------------|--------------------------------------------------------------------------|----------------|
| Send EB                         | 0         | 2 bytes                | Sending period     | 0~65535                                                                  | second         |
| Configure Frequency             | 1         | 1 byte                 | Frequency number   | (0,11~26, when frequency number is set to 0, channel hopping is enabled) |                |
| Send KA                         | 2         | 2 bytes                | Sending period     | 0~65535                                                                  | millisecond    |
| Send DIO                        | 3         | 2 bytes                | Sending period     | 0~65535                                                                  | millisecond    |
| Send DAO                        | 4         | 2 bytes                | Sending period     | 0~65535                                                                  | millisecond    |
| Set Rank Value                  | 5         | 2 bytes                | Rank               | 0~65535                                                                  |                |
| Enable/Disable Security         | 6         | 1 byte                 | Option             | True(enable) False(disable)                                              |                |
| Set Slotframe Size              | 7         | 2 bytes                | Slotframe length   | 0~65535                                                                  |                |
| Enable/Disable ACK Transmission | 8         | 1 byte                 | Option             | True(enable) False(disable)                                              |                |
| Issue a 6PADDPacket             | 9         | Multiplebytes          | 6P command content | Refer to 6P command content specification                                |                |
| Issue a 6PDELETEPacket          | 10        | Multiplebytes          | 6P command content | Refer to 6P command content specification                                |                |
| Issue a 6PRELOCATEPacket        | 11        | Multiplebytes          | 6P command content | Refer to 6P command content specification                                |                |
| Issue a 6PCOUNTPacket           | 12        | Multiplebytes          | 6P command content | Refer to 6P command content specification                                |                |
| Issue a 6PLISTPacket            | 13        | Multiplebytes          | 6P command content | Refer to 6P command content specification                                |                |
| Issue a 6PCLEARPacket           | 14        | 0                      | 6P command content | Refer to 6P command content specification                                |                |
| Set Slot Duration               | 15        | 2bytes                 | Duration           | 0~65535                                                                  | Ticks (30.5us) |
| Enable/Disable 6p Response      | 16        | 1 byte                 | Option             | True(enable) False(disable)                                              |                |

The 6P command content is composed by 7 different fields. Those fields presents in 6P command content with specific order showing below:

| Fields            | Length        | Comments                                                              | Presented 6P command              |
|-------------------|---------------|-----------------------------------------------------------------------|-----------------------------------|
| cellOptions       | 1 byte        | the value can be 1,2 or 7, which indicates tx, rx or shared cell type | All 6P commands except 6PCLEAR    |
| numCell           | 1 byte        | the number of cell to be added, deleted or relocated                  | 6PADD, 6PDELETE,6PRELOCATE        | 
| celllist_delete   | 1~3 bytes     | cell list to be deleted, each cell occupy one byte                    | 6PDELETE,6PRELOCATE               |
| celllist_add      | 1~3 bytes     | cell list to be added,   each cell occupy one byte                    | 6PADD, 6PRELOCATE                 |
| listoffset        | 1 byte        | the offset starting with when listing cells                           | 6PLIST                            |
| maxListLen        | 1 byte        | the max cells to be listed                                            | 6PLIST                            |
| addition          | 1 byte        | dumy byte, used for completing the command                            | 6PCLEAR                           |

Any other value of CommandID not listed is treated as an error, and the command is discarded by the GD.
Beyond setting the set of parameters, the script when used with GD/root allows printing out on the screen of the laptop connected to GD/root, the received packet, and all the related information (type of packet, ASN when the packet is received, time offset, 6P return code, number of reserved cell, cell list etc); and when used with GD/sniffer, it allows parsing the captured packet.
The format of the packet is printed out on the screen of the laptop connected to GD/sniffer to verify the correctness of the packet format itself.
Vendors are free to bring their own packet sniffer, able to support similar functions to those of GD/sniffer in order to perform both interoperability and conformance tests.