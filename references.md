# References

## Normative references

The following referenced documents are necessary for the application of the present document.

- [IEEE802.15.4-2015] IEEE standard for Information Technology, IEEE std. 802.15.4e, Part. 15.4: Low-Rate Wireless Personal Area Networks (LR-WPANs) Amendment 1: MAC sublayer, April 2012.
- [RFC6550] T. Winter, P. Thubert, A. Brandt, J. Hui, R. Kelsey, P. Levis, K. Pister, R. Struik, JP. Vasseur, and R. Alexander, "RPL: IPv6 Routing Protocol for Low-Power and Lossy Networks", RFC6550, March 2012.
- [RFC6552] P. Thubert, Objective Function Zero for the Routing Protocol for Low-Power and Lossy Networks (RPL), RFC6552, March 2012.
- [RFC6553] J. Hui, and JP Vasseur, The Routing Protocol for Low-Power and Lossy Networks (RPL) Option for Carrying RPL Information in Data-Plane Datagrams, RFC6553, March 2012.
- [RFC6554] J. Hui, JP. Vasseur, D. Culler, and V. Manral, An IPv6 Routing Header for Source Routes with the Routing Protocol for Low-Power and Lossy Networks (RPL), RFC6554, March 2012.
- [RFC7252] The Constrained Application Protocol (CoAP), RFC7252, June 2014.
- [RFC8137] IEEE 802.15.4 Information Element for the IETF, RFC8137, May 2017.
- [RFC8138] IPv6 over Low-Power Wireless Personal Area Network (6LoWPAN) Routing Header, RFC8138, April 2017.
- [RFC8180] X. Vilajosana, K. Pister, T. Watteyne. Minimal 6TiSCH Configuration, RFC8180, May 2017.
- [draft-ietf-6tisch-6top-protocol] Qin Wang, Xavier Vilajosana, T. Watteyne,. 6top Protocol (6P). draft-ietf-6tisch-6top-protocol-05. May 2017.
- [draft-ietf-roll-routing-dispatch] P. Thubert, C. Bormann, L. Toutain, R Craigie, 6LoWPAN Routing Header, draft-ietf-roll-routing-dispatch-05, October 2016.
- [draft-ietf-core-object-security] Object Security of CoAP (OSCOAP), draft-ietf-core-object-security-03, May 2017.

## Informative references

- [RFC7554] T. Watteyne, M. R. Palattella, L. A. Grieco, Using IEEE802.15.4e Time-Slotted Channel Hopping (TSCH) in the Internet of Things (IoT): Problem Statement, RFC7554, May 2015.
- [draft-ietf-6tisch-architecture] P. Thubert, An Architecture for IPv6 over Time Slotted Channel Hopping, IETF 6TiSCH Working Group, draft-ietf-6tisch-architecture-11, Jan. 2017.
- [draft-ietf-6tisch-terminology] M. R. Palattella, P. Thubert, T. Watteyne, Q. Wang, Terminology in IPv6 over Time Slotted Channel Hopping, IETF 6TiSCH Working Group, draft-ietf-6tisch-terminology-08,  Dec. 2016.