## Interoperability test process

### Introduction

According to well-established test methodology, such as ETSI EG 202 237 and ETSI EG 202 568, it is possible to distinguish two different and complementary ways for testing devices which implement a given standard: Conformance and Interoperability testing.

**Conformance Testing** aims at checking whether a product correctly implements a particular standardized protocol. 
Thus, it establishes whether or not the protocol Implementation Under Test (IUT) meets the requirements specified for the protocol itself. 
For example, it tests protocol messages contents and format, as well as the permitted sequences of messages.

**Interoperability Testing** aims at checking whether a product works with other products implementing the same standards.
Thus, it proves that end-to-end functionality between (at least) two devices (from different vendors) is as required by the standard(s) on which those devices are based.

Conformance testing in conjunction with interoperability testing provides both the proof of conformance and the guarantee of interoperation.
ETSI EG 202 237 and ETSI EG 202 568 describe several approaches on how to combine these two methods.
The most common approach consists in Interoperability Testing with Conformance Checks, where reference points between the devices under test are monitored to verify the appropriate sequence and contents of protocol messages, API calls, interface operations, etc.
This will be the approach used by the 6TiSCH Plugtests.

The test session will be mainly executed between two devices from different vendors.
For some test descriptions, it may be necessary to have more than two devices involved.

### The test description proforma

The test descriptions are provided in proforma tables, which include the different steps of the Test Sequence.
The steps can be of different types, depending on their purpose:

- A **stimulus** corresponds to an event that triggers a specific protocol action on a Node Under Test (NUT), such as sending a message.
- A **configure** corresponds to an action to modify the NUT or System Under Test (SUT) configuration.
- An **IOP check** (IOP stands for "Interoperation") consists of observing that one NUT behaves as described in the standard: i.e. resource creation, update, deletion, etc.
For each IOP check in the Test Sequence, a result can be recorded.
- The overall IOP Verdict is considered "PASS" if and only if all the IOP checks in the sequence are "PASS".


In the context of **Interoperability Testing with Conformance Checks**, an additional step type, **CON checks** (CON stands for "Conformance") can be used to verify the appropriate sequence and contents of protocol messages, API calls, interface operations, etc.
In this case, **the IOP Verdict will be PASS if all the IOP checks are PASS, and CON Verdict will be PASS if all the CON checks are PASS. 
The IOP/CON Verdict will be FAIL if at least one of the IOP/CON checks is FAIL.**
