## Test configuration

### Wireshark

6TiSCH uses wireshark as a reference dissector. You can use this branch to build your version:

    https://github.com/wireshark/wireshark

As much as possible, patches adding support for 6TiSCH features will be sent to the official Wireshark repository.

### Node under Test (NUT)

In the context of 6TiSCH, and according to RFC8180, a Node Under Test is a low-power wireless node equipped with a IEEE802.15.4-compliant radio, and implementing at least:

- the IEEE802.15.4e TSCH MAC protocol
- the 6LoWPAN adaptation layer
- the RPL routing protocol

In the scope of this Test Description, a NUT also implements:

- ICMPv6 echo request/reply (ping)
- draft-ietf-6tisch-6top-protocol-05
- IPv6 over Low-Power Wireless Personal Area Network (6LoWPAN) Routing Header (RFC8138)
- IEEE 802.15.4 Information Element for the IETF (RFC8137)
- draft-ietf-6tisch-minimal-security-03
- The Constrained Application Protocol (CoAP) (RFC7252)
- draft-ietf-core-object-security-03

Additionally, the NUT is also required to implement specific functions not being defined in the draft or standard, but necessary for conducting the tests.
In the scope of this Test Description, a NUT also implements:

- a way to increase and decrease traffic load.
- a way to disable and enable 6P Response.

The traffic load of each 6N can be modified either with a button pressing event or a serial command input.
There is no specific requirement for how to implement this function as long as the node supports that.
The disabling and enabling 6P Response functions are required when conducting the timeout test (TD_6TiSCH_6P_06).
"Disable the 6P Response" means that the node does not send response even if it is available to send.
This means the node stops participating in a 6P transaction while it is ongoing.
Then, "Enable the 6P Response" operation makes the node resumes normal operation.
However, the node is only able to send the response after TIMEOUT.

### System under Test (SUT)

The System Under Test (SUT) is composed of a number of Nodes Under Test (NUTs), possibly implemented by different vendors.
To address different functional areas and groups of tests, the following SUT scenario have been defined.

#### Single hop

For most tests, the SUT is a 6TiSCH single-hop topology, including a DAGroot and a 6TiSCH Node.
For conformance tests, the DR is the golden device (GD/root).
For interoperability tests, the DR is implemented by the vendor.
In some tests, in order to verify the correct formatting of the frames exchanged between the DR and the 6N, a packet sniffer is also needed.


                   +----------------+
                   |                |
                   | Packet Sniffer |
                   |                |
                   +----------------+


      +----------+                    +-------------+
      |          | +----------------> |             |
      | DAG root |                    | 6TiSCH Node |
      |          | <----------------+ |             |
      +----------+                    +-------------+


#### Multi-hop

The multi-hop scenario includes 1 DR and 3 6Ns, forming a linear topology.
This topology is used for testing join security features.
The DR is either a GD/root or a vendor node.
For some tests, another GD/sniffer or a vendor PS is used for capturing the frames exchanged.



                                  +----------------+
                                  |                |
                                  | Packet Sniffer |
                                  |                |
                                  +----------------+


	+----------+         +-------------+         +-------------+         +-------------+
	|          | +-----> |             | +-----> |             | +-----> |             |
	| DAG root |         | 6TiSCH Node |         | 6TiSCH Node |         | 6TiSCH Node |
	|          | <-----+ |             | <-----+ |             | <-----+ |             |
	+----------+         +-------------+         +-------------+         +-------------+

#### Star

The star scenario includes 1 DR and 2 6Ns, both directly connected to the DR.
For some tests, another GD/sniffer or a vendor PS is used for capturing the frames exchanged.
This start topology is mainly used for testing 6P features.

	                      +----------------+
	                      |                |
	                      | Packet Sniffer |
	                      |                |
	                      +----------------+


	+-------------+         +----------+         +-------------+
	|             | +-----> |          | +-----> |             |
	| 6TiSCH Node |         | DAG root |         | 6TiSCH Node |
	|             | <-----+ |          | <-----+ |             |
	+-------------+         +----------+         +-------------+

## IEEE802.15.4 Default Parameters

All the tests are performed using the following setting.

### Address length

ALL IEEE802.15.4 addresses will be long (64-bit). The exception is broadcast address:

    Broadcast Address: 0xffff.

### Frame version

ALL IEEE802.15.4 frames will be of version 2 (b10).

### PAN ID compression and sequence number

All IEEE802.15.4 frames will contain the following field:

- a source address,
- a destination address,
- a sequence number,
- a destination PANID (no source PANID).

### Payload termination IE

The IE payload list termination will NOT be included in the EB.

### IANA for 6P IE related

Since they have not defined yet by IANA, for the Interop event, we use the following values:


| Abbreviation              | Meaning |
| -------------             | ------- |
| IANA_IETF_IE_GROUP_ID     | 0x05    |
| IANA_6TOP_SUBIE_ID        | 0xC9    |
| IANA_6TOP_6P_VERSION      | 0x00    |
| IANA_SFID_SF0             | 0x00    |
| IANA_6TOP_CMD_ADD         | 0x01    |
| IANA_6TOP_CMD_DELETE      | 0x02    |
| IANA_6TOP_CMD_RELOCATE    | 0x03    |
| IANA_6TOP_CMD_COUNT       | 0x04    |
| IANA_6TOP_CMD_LIST        | 0x05    |
| IANA_6TOP_CMD_CLEAR       | 0x06    |
| IANA_6TOP_RC_SUCCESS      | 0x00    |
| IANA_6TOP_RC_ERROR        | 0x01    |
| IANA_6TOP_RC_EOL          | 0x02    |
| IANA_6TOP_RC_RESET        | 0x03    |
| IANA_6TOP_RC_VER_ERR      | 0x04    |
| IANA_6TOP_RC_SFID_ERR     | 0x05    |
| IANA_6TOP_RC_GEN_ERR      | 0x06    |
| IANA_6TOP_RC_BUSY         | 0x07    |
| IANA_6TOP_RC_NORES        | 0x08    |
| IANA_6TOP_RC_CELLLIST_ERR | 0x09    |

### Slotframe length.

Unless otherwise stated, the slotframe length is set to 11 slots.

### 6top Timeout

A timeout happens when the node sending the 6P Request has not received the 6P
Response. The value of the timeout is set to 4 seconds during the tests.

### RPL Operation Mode

There are two modes for a RPL Instrance to choose for maintaning Downward
routes: Storing and Non-Storing modes. We use the Non-Storing mode during the
tests.

### Default Layer-2 Security Keys

To perform the SEC-related tests, the value of key K1 and K2 will be set to:

    K1: 0x11111111111111111111111111111111
    K2: 0x22222222222222222222222222222222

Moreover, Key Index (advertised in the auxiliary security header of the packet), will be used for K1 and K2, to enable nodes to look up the right key before decrypting.

## Secure Join Default Parameters

Secure join (SECJOIN) tests are performed using the following common parameters and configuration settings.

### Configuration

JRC is co-located with DR.

### Link-layer Requirements

#### EB Period

For all SECJOIN tests, the DR sends EBs periodically, with a fast rate (equal to 10 sec, according to IEEE802.15.4std), so that the Pledge does not need to send KAs for keeping synchronization.

#### Channel Hopping

All frames are sent on a single frequency -- channel hopping is disabled.

#### Layer 2 Security

The Layer 2 SEC option is enabled on DR and Pledge.

The keys K1 and K2 are set as specified before. They are unknown to the Pledge.

### Common Security Context

JRC and Pledge share an OSCOAP security context established out-of-band.

The Master Secret (MS) is set to:

    MS: 0xDEADBEEFCAFEDEADBEEFCAFEDEADBEEF

Algorithm is set to AES-CCM-16-64-128.

Sender and Recipient ID are set according to Pledge's EUI-64 as per draft-ietf-6tisch-minimal-security-03.

### Resources Exposed by JRC

JRC runs a CoAP (RFC7252) server exposing /j resource, that is OSCOAP protected with GET as the only allowed method.

### IANA for OSCOAP

Since Object-Security CoAP option number has not yet been defined by IANA, for the Interop event, we use the value 21.

