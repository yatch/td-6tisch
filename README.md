# 6TiSCH Interoperability Test Description

Version: 2017-08-30 12:59:39.470813

This repository forms the guidelines to lead the technical organization of the first F-Interop 6TiSCH Interoperability event, organized by ETSI and LIST, in the framework of the SORT project, funded by the H2020 F-Interop open call.
During the event, the F-Interop Platform will be used for the testing, with the technical support from Inria and OpenMote.

The Interoperability event is held in Prague, Czech Republic, on 14-15 July 2017.

This repository describes:

- The configurations used during test sessions, including the relevant parameter values of the different layers (IEEE802.15.4, TSCH, 6TiSCH, 6P, RPL).

- The interoperability test descriptions, describing the scenarios the participants follow to perform the tests.

- Guidelines on how to use the tools provided:

	- the golden device, a pre-programmed physical device to test an implementation against
	a modified version of Wireshark,
	- a packet analyzer, which includes the necessary dissectors

## Table of contents:

- [Introduction to Interoperability Test Process](intro.md)
- [Definition & Abbreviations](abbreviations.md)
- [Configuration](config.md)
- [Conventions](conventions.md)
- [F-Interop specific](f-interop.md)
- [Golden Device](golden_device.md)
- [References](references.md)
