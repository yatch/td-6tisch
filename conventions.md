# Conventions

Participant shall use their own tools for logging and analyzing messages for the "check" purpose.

The following tools are REQUIRED for executing the tests.

- **Sniffer**: An IEEE802.15.4 compliant sniffer and the relevant tools to be able to analyze packets exchanges over the air.

  _Note_: *The Plugtests organizers provide the participants with a "Golden Device" which can act as a packet sniffer device.
Participants are free, however, to use their own sniffer.*

- **Dissector**: A computer program capable of interpreting the frames captured by the packet sniffer, and verify the correct formatting of the different headers inside that frame.

  _Note_: *The Plugtests organizers provide the participants with a custom-built version of Wireshark, a popular packet analysis software, which contains the necessary dissectors.
Participants are free, however, to use their own dissector(s).*

## Test Description naming convention

All the tests described in this document, which will be performed during the Plugfest, can be classified in different groups, based on the type of features they verify.
There are 5 different groups of tests:

- Synchronization (SYN): Test that nodes can stay synchronized.
- 6TiSCH Minimal (MINIMAL): Test that the 6TiSCH Minimal Configuration is correctly implemented.
- Security L2 (SECL2): Test L2 security features (authentication, encryption).
- Security JOIN (SECJOIN): Test Join security features
- 6top protocol (6P): Tests the features of the 6P protocol that manages the schedule.

For each group, several tests are performed.
To identify each test, this TD uses a Test ID following the following naming convention:
TD_6TiSCH_<test group>_<test number within the group>